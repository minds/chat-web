import React from "react";

import PlatformPeg from "matrix-react-sdk/src/PlatformPeg";
import Login from "matrix-react-sdk/src/Login";

export default class MindsLogin extends React.PureComponent {
    static replaces = "LoginComponent";

    startSSO = () => {
        const fallbackHsUrl = this.props.fallbackHsUrl;
        const hsUrl = this.props.serverConfig.hsUrl;
        const isUrl = this.props.serverConfig.isUrl;

        const loginLogic = new Login(hsUrl, isUrl, fallbackHsUrl, {
            defaultDeviceDisplayName: this.props.defaultDeviceDisplayName,
        });

        const matrixClient = loginLogic.createTemporaryClient();

        PlatformPeg.get().startSingleSignOn(
            matrixClient,
            "sso",
            this.props.fragmentAfterLogin
        );
    };

    render() {
        this.startSSO();
        return (
            <div>
                <b>Please wait a moment...</b>
            </div>
        );
    }
}
